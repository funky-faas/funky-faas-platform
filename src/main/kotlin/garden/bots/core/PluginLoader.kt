package garden.bots.core

import garden.bots.tools.JarFileLoader
import java.net.URL


class PluginLoader {
  companion object {
    private val classLoader = JarFileLoader(arrayOf<URL>())
  }

  fun addJar(jarPathFile: String) {
    classLoader.addFile(jarPathFile)
  }

  fun loadClass(className: String): Class<*> {
    return classLoader.loadClass(className)
  }

}