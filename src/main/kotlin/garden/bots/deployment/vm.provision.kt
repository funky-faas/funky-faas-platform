package garden.bots.deployment

import arrow.core.Failure
import arrow.core.Success
import arrow.core.Try
import garden.bots.json
import garden.bots.param
import garden.bots.processes.ProcessManager
import garden.bots.processes.ProcessOptions
import garden.bots.text
import io.minio.MinioClient
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.Router
import io.vertx.kotlin.core.json.json
import io.vertx.kotlin.core.json.obj

/* ===== Deploy one Funktion from the Object Storage ===== */
fun deployFromObjectStorage(
  objectStorage: String,
  accessKey: String,
  secretKey: String,
  funktionsBucket: String,
  funktionName: String,
  funktionVersion: String,
  timeout: Int,
  processManager: ProcessManager,
  processCommandAndParameters: List<String>,
  modulesPath: String,
  pluginsPath: String
): Try<Any> {
  return Try {

    val funktionStorageId = "$funktionName:$funktionVersion"
    val deployObjectName = "$funktionStorageId:deploy.json"
    val minioClient = MinioClient(objectStorage, accessKey, secretKey)
    val stream = minioClient.getObject(funktionsBucket, deployObjectName)
    // get the type of the funktion
    val contentType = JsonObject(
      stream.bufferedReader().use { item -> item.readText() }
    ).getString("type") // defaults to UTF-8
    println("👁 > deploying $funktionStorageId ($contentType)")

    processManager.deploy(
      ProcessOptions(
        funktionName = funktionName,
        version= funktionVersion,
        type= contentType,
        timeout= timeout,
        funktionsPath= "",
        funktionDirectory= "",
        modulesPath= modulesPath,
        pluginsPath= pluginsPath,
        command= processCommandAndParameters,
        storageMode= "object-storage",
        funktionStorageId= funktionStorageId,
        accessKey= accessKey,
        secretKey= secretKey,
        funktionsBucket= funktionsBucket,
        objectStorage= objectStorage
      )
    ).pid()

  }
}

/**
 * Route (hook) API to deploy/provision a funktion
 */

// hook to generate deployment
// TODO: add authentication key in the header
// TODO: detect if it's a re deployment
fun deploymentFromObjectStorageHook(
  objectStorage: String,
  accessKey: String,
  secretKey: String,
  funktionsBucket: String,
  router: Router,
  timeout: Int,
  processManager: ProcessManager,
  processCommandAndParameters: List<String>,
  modulesPath: String,
  pluginsPath: String,
  adminToken: String
) {
  // funktionsBucket: String, funktionStorageId: String
  //router.get("/deploy/:name/:version").handler(TimeoutHandler.create(timeout.toLong()))
  //router.get("/deploy/:name/:version").blockingHandler
  router.get("/deploy/:name/:version").handler { context ->

    val funktionName = context.param("name").toString()
    val funktionVersion = context.param("version").toString()

    println("ATTEMPT TO DEPLOY $funktionName:$funktionVersion")

    val deploy = {
      println("DEPLOYING $funktionName:$funktionVersion")


      //val funktionStorageId = funktionStorageId.split(":").subList(0,2).joinToString(":")
      //context.text("👋 > deploying $funktionName:$funktionVersion")

      deployFromObjectStorage(
        objectStorage= objectStorage,
        accessKey= accessKey,
        secretKey= secretKey,
        funktionsBucket= funktionsBucket,
        funktionName= funktionName,
        funktionVersion= funktionVersion,
        timeout= timeout,
        processManager= processManager,
        processCommandAndParameters= processCommandAndParameters,
        modulesPath= modulesPath,
        pluginsPath= pluginsPath
      ).let {
        when(it) {
          is Failure -> {
            println(it.exception.printStackTrace())
            context.json(json {
              obj(
                "funktionStorageId" to "$funktionName:$funktionVersion",
                "status" to "failure",
                "error" to it.exception.message
              )
            })
          }
          is Success -> {
            val res = json {
              obj(
                "funktionStorageId" to "$funktionName:$funktionVersion",
                "deployed" to "success"
              )
            }
            context.json(res)
            println(res.encodePrettily())
          }
        } // when
      } // let
    } // deploy()


    val headerAdminToken = context.request().getHeader("FUNKTIONS_ADMIN_TOKEN")


    when(headerAdminToken) {
      adminToken -> deploy()
      else -> {
        println("OUCH BAD TOKEN $funktionName:$funktionVersion")
        context.json(json {
          obj(
            "funktionStorageId" to "$funktionName:$funktionVersion",
            "status" to "failure",
            "error" to "bad admin token"
          )
        })
      }
    }

  } // handler
}