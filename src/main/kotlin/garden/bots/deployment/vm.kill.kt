package garden.bots.deployment

/*
* WIP 🚧
*
* - after killing the funktion, remove it from the list
*
* */

import arrow.core.Failure
import arrow.core.Success
import arrow.core.Try
import garden.bots.json
import garden.bots.param
import garden.bots.processes.ProcessFunktion
import garden.bots.processes.ProcessManager
import io.vertx.ext.web.Router
import io.vertx.kotlin.core.json.json
import io.vertx.kotlin.core.json.obj


fun killOldestFunktion(processManager: ProcessManager, name: String, version: String): Try<String> {
  return Try {
    processManager.processInstances(name, version).first().funktionStorageId

  }
}

fun killLatestFunktion(processManager: ProcessManager, name: String, version: String): Try<String> {
  return Try {
    processManager.processInstances(name, version).last().funktionStorageId

  }
}

fun killFunktionByPid(processManager: ProcessManager, name: String, version: String, pid: Long): Try<String> {
  return Try {

    val p = processManager
      .processInstances(name, version)
      .find { item -> item.process.pid() == pid }

    (p?.process as Process).destroyForcibly()

    p.funktionStorageId
  }
}


fun killAllInstancesFunktion(processManager: ProcessManager, name: String, version: String): Try<List<String>> {
  return Try {

    val instances = processManager.processInstances(name, version).map { item ->
      "${item.funktionStorageId}(pid: ${item.process.pid()})"
    }

    processManager.processInstances(name, version).filter { item -> item.running }.forEach { item ->
      println("🗑 Destroying $name $version ${item.httpPort} (pid:${item.process.pid()})")


      item.process.destroyForcibly()
      item.toBeDeleted = true
      item.killed = true
      item.running = false
    }

    instances

  }
}


fun killFunktionHook(
  router: Router,
  processManager: ProcessManager,
  adminToken: String
) {

  /*
  router.delete("/processes/kill/oldest/:name/:version").handler { context ->

    killOldestFunktion(processManager, context.param("name").toString(), context.param("version").toString()).let {
      when(it) {
        is Failure -> {}
        is Success -> {
          context.json(json {
            obj(
              "result" to it.value,
              "instance" to "oldest",
              "message" to "bye"
            )
          })
        }
      }
    }
  }
  */

  /*
  router.delete("/processes/kill/latest/:name/:version").handler { context ->
    killLatestFunktion(processManager, context.param("name").toString(), context.param("version").toString()).let {
      when(it) {
        is Failure -> {}
        is Success -> {
          context.json(json {
            obj(
              "result" to it.value,
              "instance" to "latest",
              "message" to "bye"
            )
          })
        }
      }
    }
  }
  */

  router.delete("/processes/kill/all/:name/:version").handler { context ->

    val headerAdminToken = context.request().getHeader("FUNKTIONS_ADMIN_TOKEN")


    when(headerAdminToken) {
      adminToken -> {
        killAllInstancesFunktion(processManager, context.param("name").toString(), context.param("version").toString()).let {
          when(it) {
            is Failure -> {}
            is Success -> {
              context.json(json {
                obj(
                  "result" to it.value,
                  "instance" to "latest",
                  "message" to "bye"
                )
              })
            }
          }
        }
      }
      else -> {
        println("OUCH BAD TOKEN when killing ${context.param("name")}:${context.param("version")}")
        context.json(json {
          obj(
            "funktionStorageId" to "${context.param("name")}:${context.param("version")}",
            "status" to "failure",
            "error" to "bad admin token"
          )
        })
      }
    }


  }

  /*
  router.delete("/processes/kill/pid/:name/:version/:pid").handler { context ->
    killFunktionByPid(processManager, context.param("name").toString(), context.param("version").toString(), context.param("pid") as Long).let {
      when(it) {
        is Failure -> {}
        is Success -> {
          context.json(json {
            obj(
              "result" to it.value,
              "instance" to "latest",
              "message" to "bye"
            )
          })
        }
      }
    }
  }
  */

}