package garden.bots.deployment

import arrow.core.Failure
import arrow.core.Success
import arrow.core.Try
import garden.bots.processes.ProcessManager
import garden.bots.processes.ProcessOptions
import io.vertx.core.json.JsonObject
import java.io.File

// TODO: return someting
fun deployFunktionsFromFileSystemWhenStarting(
  funktionsPath: String,
  modulesPath: String,
  pluginsPath: String,
  timeout: Int,
  processManager: ProcessManager,
  processCommandAndParameters: List<String>
) {

  File(funktionsPath).listFiles().filter { item -> item.isDirectory }.forEach {

    Try {
      val path = it.canonicalPath.toString()
      val funktionDirectory = it.canonicalPath.substringAfterLast("/")
      val jsonFile = File("$path/deploy.json").readText(Charsets.UTF_8)
      val deployInformation = JsonObject(jsonFile)

      // Linter here?
      //val mainSourceCode = File("$path/main.kt").readText(Charsets.UTF_8)

      /* funktion deployment */
      val funktionName = deployInformation.getString("name")
      val funktionVersion = deployInformation.getString("version")
      //val funktionTimeout = deployInformation.getInteger("timeout")
      val functionType = deployInformation.getString("type")

      println("🤖 > Deploying $funktionName($funktionVersion)")

      /**
       *  Tuning For a Small Memory Footprint:
       *  see https://docs.oracle.com/cd/E13150_01/jrockit_jvm/jrockit/geninfo/diagnos/tune_footprint.html
       */

      processManager.deploy(processOptions =
      ProcessOptions(
        funktionName= funktionName,
        version= funktionVersion,
        type= functionType,
        timeout= timeout,
        funktionsPath= funktionsPath,
        funktionDirectory= funktionDirectory,
        modulesPath= modulesPath,
        pluginsPath= pluginsPath,
        command= processCommandAndParameters,
        storageMode= "file-system",
        funktionStorageId= "",
        accessKey= "",
        secretKey= "",
        funktionsBucket= "",
        objectStorage= ""
      )
      )

    }.let { processManager ->
      when(processManager) {
        is Failure -> println("😡 [file system] >  ${processManager.exception.message}")
        is Success -> {}
      }
    }

  }

}