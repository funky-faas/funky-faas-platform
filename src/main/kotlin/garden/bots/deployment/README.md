
### Réflexions

- si je déploie (si j'utilise deployFromObjectStorage) c'est forcément une nouvelle instance, mais qui va remplacer la précédente (ou les précédentes, si j'ai scalé)
- du coup il faut que je sache identifier si c'est une nouvelle funktion ou pas
  - sauf si je décide que deployFromObjectStorage c'est forcément une nouvelle version
  - cela laisse quand même la possibilité de redéployer sous la même instance mais avec des différences
  - MD5: https://stackoverflow.com/questions/3010071/how-to-calculate-md5-checksum-on-directory-with-java-or-groovy
- il faut que je regroupe les instances de funktion avec un identifiant commun (mais pas unique)

=> à faire en 1er:
  - scale avec un groupe name et un petit nom pour chacune des machines
  - en fait le groupe name c'est le nom de la fonction et sa version (qui peut être la branche)
  - 👋 ⚠️ TODO => changer le terme version par reference
  
  => donc si je déploie avec la même référence: je remplace
  => si je déploie avec une référence différente: je déploie "à côté"
  => si je scale je je déploie la même référence
  
  => donc en 1er, faire mon refactoring (ou décider que version et référence c'est la même chose)