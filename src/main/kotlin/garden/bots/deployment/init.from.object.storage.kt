package garden.bots.deployment

import arrow.core.Failure
import arrow.core.Success
import arrow.core.Try
import garden.bots.processes.ProcessManager
import garden.bots.processes.ProcessOptions
import io.minio.MinioClient
import io.vertx.core.json.JsonObject

fun deployFunktionsFromObjectStorageWhenStarting(
  objectStorage: String,
  funktionsBucket: String,
  accessKey: String,
  secretKey: String,
  modulesPath: String,
  pluginsPath: String,
  timeout: Int,
  processManager: ProcessManager,
  processCommandAndParameters: List<String>
) {

  Try {
    val minioClient = MinioClient(objectStorage, accessKey, secretKey)
    val isExist = minioClient.bucketExists(funktionsBucket)
    if (isExist) {
      println("  👋 > $funktionsBucket Bucket already exists.")
    } else {
      minioClient.makeBucket(funktionsBucket)
    }
    minioClient
  }.let {
    when(it) {
      is Failure -> println("😡 [object storage] > ${it.exception.message}")
      is Success -> {
        val minioClient = it.value
        minioClient.listObjects(funktionsBucket).forEach { obj ->
          val objectName = obj.get().objectName()
          if(objectName.split(":").last() == "deploy.json") {

            val funktionName = objectName.split(":")[0]
            val funktionVersion = objectName.split(":")[1]
            val funktionStorageId = objectName.split(":").subList(0,2).joinToString(":")

            val deployObjectName = "$funktionStorageId:deploy.json"
            val stream = minioClient.getObject(funktionsBucket, deployObjectName)

            val contentType = JsonObject(
              stream.bufferedReader().use { reader -> reader.readText() }
            ).getString("type") // defaults to UTF-8

            println("👁 > funktion from object storage: $funktionStorageId ($contentType)")


            processManager.deploy(
              ProcessOptions(
                funktionName= funktionName,
                version= funktionVersion,
                type= contentType,
                timeout= timeout,
                funktionsPath= "",
                funktionDirectory= "",
                modulesPath= modulesPath,
                pluginsPath= pluginsPath,
                command= processCommandAndParameters,
                storageMode= "object-storage",
                funktionStorageId= funktionStorageId,
                accessKey= accessKey,
                secretKey= secretKey,
                funktionsBucket= funktionsBucket,
                objectStorage= objectStorage
              )
            )

          }

        }
      }
    }
  }

}