package garden.bots.allowed

import io.vertx.ext.web.Router

interface RoutePlugin {
  fun define(router: Router)
  fun about(): String
  fun version(): String
}