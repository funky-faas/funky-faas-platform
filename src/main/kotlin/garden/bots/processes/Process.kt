package garden.bots.processes

import arrow.core.None
import arrow.core.Option
import arrow.core.Some
import io.vertx.circuitbreaker.CircuitBreaker
import io.vertx.core.Vertx
import io.vertx.ext.web.client.WebClient
import io.vertx.kotlin.circuitbreaker.CircuitBreakerOptions
import java.util.*
import kotlin.collections.ArrayList

data class ProcessOptions(
  val funktionName: String,
  val version: String,
  val type: String,
  //val httpPort: Int,
  val timeout: Int,
  val funktionsPath: String,
  val funktionDirectory: String,
  val modulesPath: String,
  val pluginsPath: String,
  val command: List<String>,
  val storageMode: String,
  val funktionStorageId: String,
  val accessKey: String,
  val secretKey: String,
  val funktionsBucket: String,
  val objectStorage: String
)

// TODO: a lot of fields can be removed [refactoring]
data class ProcessFunktion(
  val funktionName: String,
  val version: String,
  val type: String,
  val httpPort: Int,
  val timeout: Int,
  val funktionsPath: String,
  val funktionDirectory: String,
  val modulesPath: String,
  val pluginsPath: String,
  val command: List<String>,
  val process: Process,
  val storageMode: String,
  val funktionStorageId: String,
  val accessKey: String,
  val secretKey: String,
  val funktionsBucket: String,
  val objectStorage: String,
  val startedAt: Date,
  var toBeDeleted: Boolean = false, // used when you deploy the same funktion (same name + same version/ref)
  var running: Boolean = false,
  var killed: Boolean = false
)

class ProcessManager(
  var httpPortCounter: Int,
  val funktionsHostName: String,
  val webClient: WebClient,
  val vertx: Vertx
) {
  companion object {
    //private val processes = HashMap<String,MutableList<ProcessFunktion>>()
    private val processes = HashMap<String,ArrayList<ProcessFunktion>>()
  }
  //TODO: Try ...

  fun deploy(processOptions: ProcessOptions): Process {

    val builder = ProcessBuilder(processOptions.command)
    val env = builder.environment()

    /* === Environment variables passed to the funky-faas VMs === */
    //env["VM_HTTP_PORT"] = processOptions.httpPort.toString()
    env["VM_HTTP_PORT"] = this.httpPortCounter.toString()
    env["VM_TIMEOUT"] = processOptions.timeout.toString()
    env["VM_FUNKTIONS_PATH"] = processOptions.funktionsPath
    env["VM_FUNKTION_DIRECTORY"] = processOptions.funktionDirectory
    env["VM_MODULES_PATH"] = processOptions.modulesPath
    env["VM_PLUGINS_PATH"] = processOptions.pluginsPath
    env["VM_STORAGE_MODE"] = processOptions.storageMode
    env["VM_FUNKTION_STORAGE_ID"] = processOptions.funktionStorageId
    env["VM_ACCESS_KEY"] = processOptions.accessKey
    env["VM_SECRET_KEY"] = processOptions.secretKey
    env["VM_FUNKTIONS_BUCKET"] = processOptions.funktionsBucket
    env["VM_OBJECT_STORAGE"] = processOptions.objectStorage

    builder.redirectError(ProcessBuilder.Redirect.INHERIT)
    builder.redirectOutput(ProcessBuilder.Redirect.INHERIT)

    Option.fromNullable(processes["${processOptions.funktionName}:${processOptions.version}"]).let {
      when(it) {
        is None -> {
          processes["${processOptions.funktionName}:${processOptions.version}"] = ArrayList<ProcessFunktion>()
        }
        is Some -> {}
      }
    }

    // search for already existing funktion instance with the same name and the same version
    // and mark all found instances to be deleted
    processes["${processOptions.funktionName}:${processOptions.version}"]?.forEach {
      instanceOfProcessFunktion -> instanceOfProcessFunktion.toBeDeleted=true
    }

    // - start the new process
    // then, do some health checks
    // when health checks are ok
    // - create processFunktion and add it to processes list
    // - remove all old JVM function (kill and remove from the list)
    // Deployment has started (new JVM is launched)
    val process = builder.start()

    // here TODO: health checks with "home made" circuit breaker

    Checker(
      delay = 5000, // env
      maxCount= 20, // env
      httpPort= this.httpPortCounter,
      funktionsHostName= this.funktionsHostName,
      processOptions= processOptions,
      process= process,
      webClient= webClient,
      vertx= vertx
    ) { response, processOptions, process, httpPort  ->
      when {
        response.failed() -> {

          println("😡 Deployment failed 🚑 ${processOptions.funktionName}:${processOptions.version}")

        }

        response.succeeded() -> {
          // the remote funktion is started
          val processFunktion = ProcessFunktion(
            funktionName = processOptions.funktionName,
            version = processOptions.version,
            type= processOptions.type,
            httpPort= httpPort,
            timeout= processOptions.timeout,
            funktionsPath= processOptions.funktionsPath,
            funktionDirectory= processOptions.funktionDirectory,
            modulesPath= processOptions.modulesPath,
            pluginsPath= processOptions.pluginsPath,
            command= processOptions.command,
            process= process,
            storageMode= processOptions.storageMode,
            funktionStorageId= processOptions.funktionStorageId,
            accessKey = processOptions.accessKey,
            secretKey= processOptions.secretKey,
            funktionsBucket= processOptions.funktionsBucket,
            objectStorage= processOptions.objectStorage,
            startedAt= Date(),
            running = true,
            killed = false
          )
          // add the new JVM process
          processes["${processOptions.funktionName}:${processOptions.version}"]?.add(processFunktion)

          println("🎉 Deployment successful 🍾 ${processOptions.funktionName}:${processOptions.version}")

          // kill the other JVMs instances
          // toBeDeleted==true

          processes["${processOptions.funktionName}:${processOptions.version}"]
            ?.filter { instance -> instance.toBeDeleted }
            ?.forEach { instance ->
              println("🗑 Destroying ${instance.funktionName} ${instance.version} ${instance.httpPort} (pid:${instance.process.pid()})")

              instance.process.destroyForcibly()
              instance.killed = true
              instance.running = false
              instance.toBeDeleted = true
            }

          /*
          processes["${processOptions.funktionName}:${processOptions.version}"]?.forEach {
            instanceOfProcessFunktion -> instanceOfProcessFunktion.toBeDeleted=true
          }
          */

          // remove only the process marked at toBeDeleted=true
          // this.removeFromProcessInstancesList(processOptions.funktionName, processOptions.version)

        }
      }
    }

    // http://localhost:9091/health/yo/0.0.0
    // {"checks":[{"id":"ping","status":"UP"}],"outcome":"UP"}


    /* --- zero downtime of a funktion ---
      - search for the existing funktions (with same name and same version)
        - mark the existing same funktions to be deleted
      - do health check on the new VM
        - if ok
          - add to the processes list
          - kill the other processes
    */

    this.httpPortCounter+=1

    return process
  }


  fun processInstances(funktionName: String, version: String): ArrayList<ProcessFunktion> {
    return processes["$funktionName:$version"] as ArrayList<ProcessFunktion>
  }


  fun processes(): HashMap<String,ArrayList<ProcessFunktion>> {
    return processes
  }

}