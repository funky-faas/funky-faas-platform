package garden.bots.processes

import io.vertx.core.AsyncResult
import io.vertx.core.Vertx
import io.vertx.ext.web.client.HttpResponse
import io.vertx.ext.web.client.WebClient
import java.nio.Buffer

class Checker(
  private val delay: Long,
  val maxCount: Int,
  private val httpPort: Int,
  val funktionsHostName: String,
  val processOptions: ProcessOptions,
  val process: Process,
  val webClient: WebClient,
  val vertx: Vertx,
  val endHandler: (
    AsyncResult<HttpResponse<io.vertx.core.buffer.Buffer>>,
    processOptions: ProcessOptions,
    process: Process,
    httpPort: Int
  ) -> Unit

) {
  var counter: Int = 0

  init {

    vertx.setPeriodic(delay) { timerId ->
      println("⏱ [$timerId]($counter) $httpPort ${processOptions.funktionName}-${processOptions.version}")

      webClient.get(
        this.httpPort,
        this.funktionsHostName,
        "/health/${processOptions.funktionName}/${processOptions.version}"
      ).send { response ->
        counter+=1
        println("⏰ ${response}")
        when {
          response.failed() -> {
            if(counter>maxCount) {
              println("💀😡 [$timerId]($counter) $httpPort ${processOptions.funktionName}-${processOptions.version}")
              vertx.cancelTimer(timerId)
              endHandler(response, processOptions, process, httpPort)
            }
          }
          response.succeeded() -> {
            println("🎉😁 [$timerId]($counter) $httpPort ${processOptions.funktionName}-${processOptions.version}")
            println(response.result().bodyAsString())
            vertx.cancelTimer(timerId)
            endHandler(response, processOptions, process, httpPort)
          }
        }
      }



    }
  }
}