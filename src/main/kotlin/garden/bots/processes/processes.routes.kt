package garden.bots.processes

import garden.bots.json
import garden.bots.param
import io.vertx.ext.web.Router
import io.vertx.kotlin.core.json.json
import io.vertx.kotlin.core.json.obj

fun processesRoutes(router: Router, processManager: ProcessManager) {
  router.get("/processes").handler { context ->

    context.json(json {
      obj("processes" to processManager.processes().map { item ->
        json {
          obj("instances" to item.value.map { process ->
            json {
              obj(
                "name" to process.funktionName,
                "version" to process.version,
                "port" to process.httpPort,
                "type" to process.type,
                "pid" to process.process.pid(),
                "alive" to process.process.isAlive,
                "started" to process.startedAt,
                "toBeDeleted" to process.toBeDeleted,
                "running" to process.running,
                "killed" to process.killed
              )
            }
          })
        }
      })
    })
  }

  router.get("/processes/:name/:version").handler { context ->
    val processFunktion = processManager.processInstances(context.param("name") as String, context.param("version") as String)
    // TODO do some health check + Try
    context.json(
      json {
        obj("instances" to processFunktion.map { process ->
          json {
            obj(
              "name" to process.funktionName,
              "version" to process.version,
              "port" to process.httpPort,
              "type" to process.type,
              "pid" to process.process.pid(),
              "alive" to process.process.isAlive,
              "started" to process.startedAt,
              "toBeDeleted" to process.toBeDeleted,
              "running" to process.running,
              "killed" to process.killed
            )
          }
        })
      }
    )
  }
}