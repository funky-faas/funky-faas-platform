package garden.bots.tools

import arrow.core.Failure
import arrow.core.Success
import arrow.core.Try
import garden.bots.allowed.RoutePlugin
import garden.bots.core.PluginLoader
import io.vertx.ext.web.Router
import java.io.File

fun loadEnterprisePluginsFromDisk(pluginsPath: String, router: Router) {
  // --- load plugins (jar files) ---
  File(pluginsPath).listFiles().filter { file -> file.extension =="jar" }.forEach { plugin ->
    val pluginLoader = PluginLoader()

    Try {
      pluginLoader.addJar(plugin.canonicalPath)

      val klass = pluginLoader.loadClass(plugin.nameWithoutExtension)
      val pluginInstance = klass.getConstructor().newInstance() as RoutePlugin

      println("📦 > loading ${plugin.nameWithoutExtension} from ${plugin.canonicalPath}")

      println(pluginInstance.about())

      pluginInstance.define(router)

    }.let {
      when(it) {
        is Failure -> {
          println("😡 > when loading ${plugin.canonicalPath}")
          println("😡 > ------------------------------------------------------------------------------")
          println("😡 > ${it.exception.stackTrace}")
          println("😡 > ------------------------------------------------------------------------------")
        }
        is Success -> {
          println("📦 > ${plugin.nameWithoutExtension} loaded from ${plugin.canonicalPath}")
        }
      }
    }
  }
}