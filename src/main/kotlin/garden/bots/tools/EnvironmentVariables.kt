package garden.bots.tools

import arrow.core.None
import arrow.core.Option
import arrow.core.Some

class EnvironmentVariables() {

  val timeout: Int = Option.fromNullable(System.getenv("TIMEOUT")).let { when(it) {
    is None -> 1000
    is Some -> Integer.parseInt(it.t)
  }}

  val httpPort: Int = Option.fromNullable(System.getenv("HTTP_PORT")).let { when(it) {
    is None -> 8080
    is Some -> Integer.parseInt(it.t)
  }}

  val startFunktionHttpPort: Int = Option.fromNullable(System.getenv("START_FUNKTION_HTTP_PORT")).let { when(it) {
    is None -> 9090
    is Some -> Integer.parseInt(it.t)
  }}


  val funktionsAdminToken: String = Option.fromNullable(System.getenv("FUNKTIONS_ADMIN_TOKEN")).let { when(it) {
    is None -> ""
    is Some -> it.t
  }}


  val funktionsHostName: String = Option.fromNullable(System.getenv("FUNKTIONS_HOST_NAME")).let { when(it) {
    is None -> "localhost"
    is Some -> it.t
  }}



  val funktionsPath: String = Option.fromNullable(System.getenv("FUNKTIONS_PATH")).let { when(it) {
    is None -> "${System.getProperty("user.dir")}/funktions"
    is Some -> "${System.getProperty("user.dir")}/${it.t}"
  }}

  val modulesPath: String = Option.fromNullable(System.getenv("MODULES_PATH")).let { when(it) {
    is None -> "${System.getProperty("user.dir")}/modules"
    is Some -> "${System.getProperty("user.dir")}/${it.t}"
  }}

  val pluginsPath: String = Option.fromNullable(System.getenv("PLUGINS_PATH")).let { when(it) {
    is None -> "${System.getProperty("user.dir")}/plugins"
    is Some -> "${System.getProperty("user.dir")}/${it.t}"
  }}

  val enterprisePluginsPath: String = Option.fromNullable(System.getenv("ENTERPRISE_PLUGINS_PATH")).let { when(it) {
    is None -> "${System.getProperty("user.dir")}/enterprise"
    is Some -> "${System.getProperty("user.dir")}/${it.t}"
  }}

  val jvmXms: String = Option.fromNullable(System.getenv("XMS")).let { when(it) {
    is None -> "-Xms64m"
    is Some -> it.t
  }}

  val jvmXmx: String = Option.fromNullable(System.getenv("XMX")).let { when(it) {
    is None -> "-Xmx64m"
    is Some -> it.t
  }}

  val securityManager: String = Option.fromNullable(System.getenv("SECURITY_MANAGER")).let { when(it) {
    is None -> "-Djava.security.manager"
    is Some -> it.t
  }}

  val securityPolicy: String = Option.fromNullable(System.getenv("SECURITY_POLICY")).let { when(it) {
    is None -> "-Djava.security.policy==./funky.policy"
    is Some -> it.t
  }}

  val vmPath: String = Option.fromNullable(System.getenv("VM_PATH")).let { when(it) {
    is None -> "./funky-vm-1.0-SNAPSHOT-fat.jar"
    is Some -> it.t
  }}

  val accessKey: String = Option.fromNullable(System.getenv("ACCESS_KEY")).let { when(it) {
    is None -> {
      val message = "😡 > ACCESS_KEY is null"
      println(message)
      message
    }
    is Some -> {
      if(it.t.isEmpty()) {
        val message = "😡 > ACCESS_KEY is empty"
        println(message)
      }
      it.t
    }
  }}

  val secretKey: String = Option.fromNullable(System.getenv("SECRET_KEY")).let { when(it) {
    is None -> {
      val message = "😡 > SECRET_KEY is null"
      println(message)
      message
    }
    is Some -> {
      if(it.t.isEmpty()) {
        val message = "😡 > SECRET_KEY is empty"
        println(message)
      }
      it.t
    }
  }}

  val objectStorage: String = Option.fromNullable(System.getenv("OBJECT_STORAGE")).let { when(it) {
    is None -> {
      val message = "😡 > OBJECT_STORAGE is null"
      println(message)
      message
    }
    is Some -> {
      if(it.t.isEmpty()) {
        val message = "😡 > OBJECT_STORAGE is empty"
        println(message)
      }
      it.t
    }
  }}


  val funktionsBucket: String = Option.fromNullable(System.getenv("FUNKTIONS_BUCKET")).let { when(it) {
    is None -> {
      val message = "😡 > FUNKTIONS_BUCKET is null"
      println(message)
      message
    }
    is Some -> {
      if(it.t.isEmpty()) {
        val message = "😡 > FUNKTIONS_BUCKET is empty"
        println(message)
      }
      it.t
    }
  }}

}