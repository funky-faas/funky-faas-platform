package garden.bots

import garden.bots.deployment.deployFunktionsFromFileSystemWhenStarting
import garden.bots.deployment.deployFunktionsFromObjectStorageWhenStarting
import garden.bots.deployment.deploymentFromObjectStorageHook
import garden.bots.deployment.killFunktionHook
import garden.bots.processes.ProcessManager
import garden.bots.processes.processesRoutes
import garden.bots.tools.EnvironmentVariables
import garden.bots.tools.loadEnterprisePluginsFromDisk
import io.vertx.circuitbreaker.CircuitBreaker
import io.vertx.circuitbreaker.CircuitBreakerOptions
import io.vertx.core.AbstractVerticle
import io.vertx.ext.web.Router
import io.vertx.ext.web.client.WebClient
import io.vertx.ext.web.handler.BodyHandler
import io.vertx.ext.web.handler.StaticHandler
import io.vertx.ext.web.handler.TimeoutHandler
import io.vertx.kotlin.circuitbreaker.CircuitBreakerOptions
import io.vertx.kotlin.core.http.HttpServerOptions
import io.vertx.kotlin.core.json.json
import io.vertx.kotlin.core.json.obj


class Main : AbstractVerticle() {

  override fun start() {

    println("""
     _____  __ __  ____   __  _  __ __         _____   ____   ____  _____
    |     ||  |  ||    \ |  |/ ]|  |  |       |     | /    | /    |/ ___/
    |   __||  |  ||  _  ||  ' / |  |  | _____ |   __||  o  ||  o  (   \_
    |  |_  |  |  ||  |  ||    \ |  ~  ||     ||  |_  |     ||     |\__  |
    |   _] |  :  ||  |  ||     ||___, ||_____||   _] |  _  ||  _  |/  \ |
    |  |   |     ||  |  ||  .  ||     |       |  |   |  |  ||  |  |\    |
    |__|    \__,_||__|__||__|\_||____/        |__|   |__|__||__|__| \___|

    """.trimIndent())

    val environment = EnvironmentVariables()
    val client = WebClient.create(vertx)

    val router = Router.router(vertx)
    router.route().handler(BodyHandler.create())

    // --- Load jar files ---
    loadEnterprisePluginsFromDisk(
      pluginsPath= environment.enterprisePluginsPath,
      router= router
    )

    val processManager = ProcessManager(
      httpPortCounter = environment.startFunktionHttpPort,
      funktionsHostName= environment.funktionsHostName,
      webClient= client,
      vertx = vertx
    )

    // command and parameter to launch external java processes
    val processCommandAndParameters: List<String> =listOf(
      "java", "-jar",
      environment.jvmXms,
      environment.jvmXmx,
      environment.securityManager,
      environment.securityPolicy,
      environment.vmPath
    )

    /**
     * Load funktions from the file system
     * ⚠️ prefer object storage
     */

    deployFunktionsFromFileSystemWhenStarting(
      funktionsPath= environment.funktionsPath,
      modulesPath= environment.modulesPath,
      pluginsPath= environment.pluginsPath,
      timeout= environment.timeout,
      processManager= processManager,
      processCommandAndParameters= processCommandAndParameters
    )

    deployFunktionsFromObjectStorageWhenStarting(
      objectStorage= environment.objectStorage,
      funktionsBucket= environment.funktionsBucket,
      accessKey= environment.accessKey,
      secretKey= environment.secretKey,
      modulesPath= environment.modulesPath,
      pluginsPath= environment.pluginsPath,
      timeout= environment.timeout,
      processManager= processManager,
      processCommandAndParameters= processCommandAndParameters
    )


    /** TODO:
     * - load from Redis
     * - health check of a function -> it's better to use health check for the entire platform
     * - circuit breaker for client (?)
     * */

    router.route("/*").handler(StaticHandler.create())

    // +++ define deployment hook +++
    deploymentFromObjectStorageHook(
      objectStorage= environment.objectStorage,
      accessKey= environment.accessKey,
      secretKey= environment.secretKey,
      funktionsBucket= environment.funktionsBucket,
      router= router,
      timeout= environment.timeout,
      processManager= processManager,
      processCommandAndParameters= processCommandAndParameters,
      modulesPath= environment.modulesPath,
      pluginsPath= environment.pluginsPath,
      adminToken= environment.funktionsAdminToken
    )

    // Define kill funktion hook
    killFunktionHook(
      router= router,
      processManager= processManager,
      adminToken= environment.funktionsAdminToken
    )

    // Define routes processes
    processesRoutes(
      router= router,
      processManager= processManager
    )

    router.get("/about").handler { context ->
      context.json(json {
        obj("about" to "Funky: Kotlin FaaS")
      })
    }

    /*
      curl --header "Content-Type: application/json" \
      http://localhost:9090/function/hello/0.0.2/%7B%22name%22:%22BOB%20MORANE%22%7D
    */

    /*
      curl --header "Content-Type: application/json" \
      --request POST \
      --data '{"name":"Paul"}' \
      http://localhost:9090/funktion/hello/0.0.2
    */


    // kind of reverse proxy
    router.get("/funktion/:name/:version/:params").handler(TimeoutHandler.create(environment.timeout.toLong()))
    router.get("/funktion/:name/:version/:params").blockingHandler { context ->

      // funky-faas host
      println("🌍 [platform]context.request().host(): ${context.request().host()}")

      //val vmHost = context.request().host().split(":")[0]
      val vmHost = environment.funktionsHostName
      // funky-vm host (the same as funky-faas)
      println("🌍 engine unit host: $vmHost")

      // ========== Funktion LoadBalancing ==========
      val processFunktionInstances =
        processManager.processInstances(
          context.param("name").toString(), context.param("version").toString()
        ).filter { instance -> !instance.killed }
      // loadBalancing between several instances of the same version of the funktion

      val randomInteger = (0..(processFunktionInstances.size-1)).shuffled().first()
      val processFunktion = processFunktionInstances[randomInteger]
      // ============================================


      val uri = "/funktion/${processFunktion.funktionName}/${processFunktion.version}"
      val port = processFunktion.httpPort
      //val parameters = JsonObject(context.param("params") as String)
      val parameters = context.param("params") as String

      println("$vmHost:$port$uri/${java.net.URLEncoder.encode(parameters, "UTF-8")}")

      // https will be managed by the main platform
      // TODO: deal with https
      client.get(
        port,
        vmHost,
        "$uri/${java.net.URLEncoder.encode(parameters, "UTF-8")}"
      ).send { ar ->
        if (ar.succeeded()) {
          val response = ar.result()

          when(processFunktion.type) {
            "json" -> context.json(response.bodyAsJsonObject())
            "text" -> context.text(response.bodyAsString())
            "html" -> context.html(response.bodyAsString())
          }

          println("(GET) Received response with status code ${response.statusCode()}")
        } else {
          // TODO send json
          ar.cause().printStackTrace()
          println("Something went wrong ${ar.cause().message}")
        }
      }


    }

    // TODO load balancing between the same funktions (several instances of the same version)

    router.post("/funktion/:name/:version").handler(TimeoutHandler.create(environment.timeout.toLong()))
    router.post("/funktion/:name/:version").blockingHandler { context ->
      val client = WebClient.create(vertx)
      // funky-faas host
      println("🌍 [platform]context.request().host(): ${context.request().host()}")
      val vmHost = context.request().host().split(":")[0]
      // funky-vm host (the same as funky-faas)
      println("🌍 engine unit host: $vmHost")
      //val processFunktion = processManager.process(context.param("name") as String, context.param("version") as String)

      // ========== Funktion LoadBalancing ==========
      val processFunktionInstances =
        processManager.processInstances(
          context.param("name").toString(), context.param("version").toString()
        )
      // loadBalancing between several instances of the same version of the funktion
      val randomInteger = (0..(processFunktionInstances.size-1)).shuffled().first()
      val processFunktion = processFunktionInstances[randomInteger]
      // ============================================

      val uri = "/funktion/${processFunktion.funktionName}/${processFunktion.version}"
      val port = processFunktion.httpPort
      val parameters = context.bodyAsJson

      /*
      curl --header "Content-Type: application/json" \
      --request POST \
      --data '{"name":"Paul"}' \
      http://localhost:8080/funktion/hello-world/0.0.0

      curl --header "Content-Type: application/json" \
      --request POST \
      --data '{"name":"Paul"}' \
      http://localhost:8080/funktion/yo/0.0.0
       */

      client.post(
        port,
        vmHost,
        "$uri"
      )
        .sendJsonObject(parameters) { ar ->
          if (ar.succeeded()) {
            val response = ar.result()

            when(processFunktion.type) {
              "json" -> context.json(response.bodyAsJsonObject())
              "text" -> context.text(response.bodyAsString())
              "html" -> context.html(response.bodyAsString())
            }

            println("(POST) Received response with status code ${response.statusCode()}")
          } else {
            // TODO send json
            ar.cause().printStackTrace()
            println("Something went wrong ${ar.cause().message}")
          }
        }

    }

    vertx.createHttpServer(HttpServerOptions(port = environment.httpPort))
      .requestHandler(router)
      .listen { ar -> when {
          ar.failed() -> println("😡 Houston?")
          ar.succeeded() -> println("😃 🌍 started on ${environment.httpPort}")
      }}
  }

}
