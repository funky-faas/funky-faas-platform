package garden.bots

import io.vertx.core.json.JsonObject
import io.vertx.ext.web.RoutingContext

// TODO: add more content types

fun RoutingContext.json(jsonObject: JsonObject) {
  this.response().putHeader("content-type", "application/json;charset=UTF-8").end(jsonObject.encodePrettily())
}

fun RoutingContext.text(content: String) {
  this.response().putHeader("content-type", "text/plain;charset=utf-8").end(content)
}

fun RoutingContext.html(content: String) {
  this.response().putHeader("content-type", "text/html; charset=utf-8").end(content)
}

fun RoutingContext.param(key: String) :Any {
  return this.request().getParam(key)
}
