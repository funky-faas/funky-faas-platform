# Funky FaaS Platform

## SetUp

- copy `funky-faas-1.0-SNAPSHOT-fat.jar` in a directory (it's the Funky Faas application)
- copy `funky-vm-1.0-SNAPSHOT-fat.jar` (it's the "funktion" JVM) in a directory (it can be the same as Funky Faas application)
- start **Minio**, like that:

```shell
export MINIO_ACCESS_KEY=battlestar
export MINIO_SECRET_KEY=galactica
export MINIO_DOMAIN=faas-storage.test
./minio server ./storage
```

## Run it

```shell
export FUNKTIONS_HOST_NAME="localhost"
export HTTP_PORT=8080
export START_FUNKTION_HTTP_PORT=9090
export FUNKTIONS_PATH="funktions"
export MODULES_PATH="modules"
export PLUGINS_PATH="plugins"
export ENTERPRISE_PLUGINS_PATH="enterprise"
export XMS="-Xms64m"
export XMX="-Xmx64m"
export SECURITY_MANAGER="-Djava.security.manager"
export SECURITY_POLICY="-Djava.security.policy==./funky.policy"
export VM_PATH="../funky-vm/funky-vm-1.0-SNAPSHOT-fat.jar"

export ACCESS_KEY="battlestar"
export SECRET_KEY="galactica"
export FUNKTIONS_BUCKET="funky-sandbox"
export OBJECT_STORAGE="http://localhost:9000"
java -jar -Djava.security.manager -Djava.security.policy==./funky.policy ./funky-faas-1.0-SNAPSHOT-fat.jar
```

## How to

> WIP

