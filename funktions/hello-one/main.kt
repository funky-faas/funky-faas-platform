import io.vertx.core.json.JsonObject
import garden.bots.allowed.*

{params: JsonObject ->

  class Buster {
    fun hello() {
      println("Hello 👋, I'm Spocky.kt 🐰")
    }
  }

  class Elmira {
    fun hello() {
      println("Hello, I'm Elmira")
    }
  }

  garden.bots.core.Kompilo().executeFunktion("hello","0.0.2",JsonObject().put("name","BoB"))
  // by default the 0.0.0 version is called ... 🤔

  val buster = Buster()
  buster.hello()

  val elmira = Elmira()
  elmira.hello()

  // No need to import Plugin because it's at the root of the project
  val message = Plugin("wonderful.things.DarthVader").call("getQuote") as String
  println(message)

  println(Plugin("wonderful.things.Yoda").call("hello") )

  JsonObject().put("message","👋 hello").put("ohohoh", merryChristmas())

}