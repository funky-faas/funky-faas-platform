import io.vertx.core.json.JsonObject
import garden.bots.allowed.*

{params: JsonObject ->

  val message = Plugin("star.trek.Spocky").call("hi") as String

  JsonObject().put("message", "👋 yo").put("spocky", message)

}