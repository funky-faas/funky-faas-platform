import io.vertx.core.json.JsonObject
import garden.bots.allowed.*

{params: JsonObject ->

  JsonObject().put("message","👋 hello again " + params.getString("name"))
}