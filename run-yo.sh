#!/usr/bin/env bash

export VM_STORAGE_MODE="file-system"
export VM_HTTP_PORT="9093"
export VM_TIMEOUT="1000"
export VM_FUNKTION_DIRECTORY="yo"
java -jar -Djava.security.manager -Djava.security.policy==./funky.policy ./funky-vm-1.0-SNAPSHOT-fat.jar

