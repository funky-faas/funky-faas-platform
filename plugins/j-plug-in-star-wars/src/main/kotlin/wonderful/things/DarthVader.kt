package wonderful.things


fun ArrayList<String>.addItem(what: String): ArrayList<String> {
  this.add(what)
  return this
}

class DarthVader {

  private val quotes = ArrayList<String>()
    .addItem("Perhaps I can find new ways to motivate them.")
    .addItem("The Emperor will show you the true nature of the Force. He is your Master now.")
    .addItem("He will join us or die, Master.")

  fun getQuote(): String {
    return quotes.shuffled().first()
  }

  fun getQuote(idx:Int): String {
    return quotes[idx]
  }
}